<img width="120" align="right" src="assets/img/ncar-logo.svg" alt="logo"/><sup>NeuroCar / Tools / Jupyter </sup>

# vtsearch 4

Download VehicleTrace data from NeuroCar BackOffice 4 using given search criteria and and store locally

---

<sup>*&copy; NeuroCar 2019-2020 - https://www.neurocar.pl, by Cezary Dołęga*</sup>